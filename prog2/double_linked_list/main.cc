#include <iostream>

using namespace std;

struct elem
{
    int val;
    elem* bef;
    elem* aft;
};

typedef elem* list;

//restituisce il valore dell'elemento passato
int head_val(elem* p){return p->val;}

//restituisce l'elemento successivo a quello passato
elem* aft_val(elem* p){return p->aft;}

//restituisce l'elemento precedente a quello passato
elem* bef_val(elem* p){return p->bef;}

//aggiunge i valori in testa
list insert_element(list head, elem* new_elem, elem* succ)
{
    //cout << "INSERIMENTO" << endl;
    new_elem->aft = succ;
    new_elem->bef = NULL;
    //cout << "INSERITO" << endl << "COLLEGAMENTO" << endl;
    if(succ != NULL)
    {
        //cout << "COLLEGANDO VECCHIO CON NUOVO!" << endl;
        succ->bef = new_elem;
        //cout << "COLLEGATO " << succ->val << " CON " << succ->bef->val << endl; 
    }
        
    //cout << "COLLEGATO!" << endl;
    return new_elem;
}

//crea la lista 
list create_list(int n)
{
    list head = NULL;
    elem* bef = NULL;       //elemento inserito per primo
    elem* aft = NULL;       //elemento inserito successivamente
    for(int i=0; i<n; i++)
    {
        bef = new elem;
        cout << "Inserire valore: ";
        cin >> bef->val;
        //cout << "INSERISCO ELEMENTO " << bef->val << endl;
        head = insert_element(head, bef, aft);
        aft = bef;
    }
    return head;
}

void print_from_head(list head)
{
    int c = 1;
    while(head != NULL)
    {
        cout << "[" << c << "]: " << head_val(head) << endl;
        head = aft_val(head);
        c++;
    }
    return;
}

void print_from_feet(list head)
{
    int c=1;
    while(aft_val(head) != NULL)
        head = aft_val(head);
    
    //cout << "ULTIMO ELEMENTO: " << head->val << endl;
    while(head != NULL)
    {
        cout << "[" << c << "]: " << head_val(head) << endl;
        head = bef_val(head);
        c++;
    }
    return;
}

elem* search_elem(list head, int val)
{
    while (head != NULL)
    {
        if(head_val(head) == val)
        {
            //cout << "TROVATO!" << endl;
            return head;
        }
        head = aft_val(head);
    }
    return NULL;
}

list delete_elem(list head, elem* p)
{
    if(p == NULL)
        return head;
    if(head == p)
    {
        if(aft_val(head) != NULL)
        {
            head = aft_val(head);
            head->bef = NULL;
        }
        else
            head = NULL;
    }
    else
    {
        //cout << "P->AFT: " << (p->aft)->val << " ORA PUNTA PRECEDENTEMENTE " << (p->bef)->val << endl;
        if(aft_val(p) !=NULL)
        {
            //se sono qui significa che ha dei valori successivi e anche dei valori precedenti (perché non è head)
            (p->aft)->bef = bef_val(p);
            (p->bef)->aft = aft_val(p);
        }
        else
        {
            //se sono qui significa che sono l'ultimo valore
            (p->bef)->aft = NULL;
        }
    }
    delete p;
    return head;
}

void delete_list(list &head)
{
    while(head != NULL)
        head = delete_elem(head, head);
    return;
}

int main()
{
    list head = NULL;
    int n;
    do
    {
        cout << "Inserire grandezza lista: ";
        cin >> n;
    } while (n<=0);

    head = create_list(n);
    print_from_head(head);
    cout << endl << endl;
    print_from_feet(head);

    cout << "Cercare elemento: ";
    cin >> n;
    head = delete_elem(head, search_elem(head, n));
    print_from_head(head);
    cout << endl << endl;
    print_from_feet(head);

    delete_list(head);
    cout << "HEAD: " << head << endl;
}