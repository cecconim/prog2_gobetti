#include <iostream>

using namespace std;

int fact(int n)
{
    if(n==1)
        return 1;
    else
        return n*fact(n-1);
}

int fib(int n)
{
    if(n == 0 || n==1)
        return n;
    else
        return fib(n-1)+fib(n-2);
}

double esp(double val, int n)
{
    if(n==0)
        return 1;
    if(n==1)
        return val;
    return val*esp(val, n-1);
}

void print_to_1(int n)
{
    if(n==0)
        return;
    cout << "N: " << n << endl;
    return print_to_1(n-1);
}

void print_from_1(int n)
{
    if(n==0)
        return;
    print_from_1(n-1);
    cout << "N: " << n << endl;
}


int main()
{
    int n, n1;
    double val;
    /*cout << "Inserire numero per calcolare fattoriale: ";
    cin >> n;
    cout << "Fattoriale di " << n << ": " << fact(n) << endl;*/

    /*cout << "Inserire numero per calcolare fibonacci: ";
    cin >> n;
    n1 = fib(n-1);
    n = fib(n);
    val = n/(double)n1;
    cout << "Sezione AUREA: " << val << endl;*/

    /*cout << "Inserire valore: ";
    cin >> val;
    cout << "Inserire esponente: ";
    cin >> n;
    cout << "Fattoriale: " << esp(val, n) << endl;*/

    cout << "Inserire valore: ";
    cin >> n;
    //print_to_1(n);
    print_from_1(n);
}