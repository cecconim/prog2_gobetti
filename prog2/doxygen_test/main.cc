/**
 * @file main.cc
 * @author your name (you@domain.com)
 * @brief 
 * @version 0.1
 * @date 2022-09-12
 * @copyright Copyright (c) 2022
 * 
 */
#include <iostream>
using namespace std;

/**
 * Descrizione breve funzione.
 * Descrizione dettagliata funzione bla bla bla
 * 
 * @return int
 */
int main()
{
    /**
     * Descrizione del main
     */
    cout << "Hello!" << endl;
    cout << "Weee" << endl;
    cout << "WW " << endl;
}