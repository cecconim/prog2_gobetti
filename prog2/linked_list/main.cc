#include <iostream>

using namespace std;

struct elem
{
    int val;
    elem* pun;
};

//list è un nuovo tipo (cioè un puntatore di elem)
typedef elem* list;

//ritorna il valore dell'elemento che gli passo
int head_val(elem* p){return p->val;}

//ritorna il puntatore dell'elemento che gli passo
elem* tail_val(elem* p){return p->pun;}

//inserisco un elemento in testata
list insert_elem(list head, elem* p)
{
    //il nuovo elemento punta al (vecchio) ultimo elemento inserito
    p->pun = head;
    //ritorno il puntatore del nuovo elemento che sarà il nuovo puntatore di head
    return p;
}

//creo la lista con n elementi
list create_list(int n)
{
    list head = NULL;
    elem* p = NULL;
    for(int i=0; i<n; i++)
    {
        p = new elem;
        cout << "Elemento: ";
        cin >> p->val;
        head = insert_elem(head, p);
    }
    return head;
}

//stampo lista (dalla testa)
void print_list(list head)
{
    int n=0;
    while(head != NULL)
    {
        cout << "Valore[" << n+1 << "]: " << head_val(head) << endl;
        n++;
        head = tail_val(head);
    }
    return;
}

//cancello l'elemento passato
list delete_elem(list head, elem* p)
{
    if(p == NULL)
        return head;
    //se puntano alla stessa cosa, significa che p è l'elemento in testa
    if(head == p)
    {
        head = tail_val(head);
    }
    else
    {
        //faccio una copia di head
        list head_2 = head;
        //finché non sono nel posto prima del valore che cerco, vado avanti
        while(tail_val(head_2) != p)
        {
            head_2 = tail_val(head_2);
        }
        //qui head_2 è nel posto appena prima di p
        //significa che dovrò fare un'operazione: 
        //il puntatore dell'elemento prima di p dovrà puntare all'elemento dopo di p (così la lista rimane concatenata)
        head_2->pun = tail_val(p);
    }
    delete p;
    return head;
}

//cancello l'intera lista
void delete_list(list &head)
{
    while(head != NULL)
    {
        head = delete_elem(head, head);
    }
}

elem* search_elem(list head, int val)
{
    while (head != NULL)
    {
        if(head_val(head) == val)
        {
            return head;
        }
        head = tail_val(head);
    }
    return NULL;
}

void insert_element_in_position(list head, elem* p, int n)
{
    int c = 1;
    //list head_2 = head;

    while(head != NULL)
    {
        //cout << "C: " << c << "    N: " << n << endl;
        if(c+1 == n)                      //sono nella posizione PRIMA di quella che mi piace!
        {
            head->pun = insert_elem(tail_val(head), p); //allora il mio nuovo p punterà a quello che punta la posizione prima
            break;                                      //mentre la posizione prima (cioè head) punterà al nuovo p
        }
        head = tail_val(head);          //scorro la lista
        c++;
    }
}

void swap(list head, elem* p1, elem* p2)
{
    cout << "P1: " << head_val(p1) << endl << "P2: " << head_val(p2) << endl;
    int swap_t = head_val(p1);
    p1->val = head_val(p2);
    p2->val = swap_t;
}

list copy(list head)
{
    list l2 = NULL;
    elem* curr;
    elem* prec = NULL;
    while(head != NULL)
    {
        curr = new elem;
        curr->val = head_val(head);
        curr->pun=NULL;
        if(prec == NULL)
            l2 = curr;              //se prec è null, significa che è il primo elemento che metto: quindi l2 è la testa della lista
        else
            prec->pun = curr;       //il precedente punta a quello appena creato
        prec = curr;
        head = tail_val(head);
    }
    return l2;
}

int main()
{
    list head = NULL;          //testa della lista
    
    int n=0;
    do
    {
        cout << "Inserire grandezza lista: ";
        cin >> n;
    } while (n<=0);
    
    head = create_list(n);
    print_list(head);
    
    /*cout << "Valore da cercare ed eliminare: ";
    cin >> n;
    head = delete_elem(head, search_elem(head, n));
    print_list(head);

    delete_list(head);
    print_list(head);
    cout << endl << endl << head << endl;*/

    /*int val = 0;
    cout << "Inserire elemento in posizione(parte da 1): ";
    cin >> n;
    cout << "Che valore inserire? ";
    cin >> val;
    cout << endl << endl;
    elem* p = new elem;
    p->val = val;
    if(n<=1)
    {
        head = insert_elem(head, p);
    }
    else
    {
        insert_element_in_position(head, p, n);
    }*/

    /*int n1, n2;
    cout << "Valore 1 SWAP: ";
    cin >> n1;
    cout << "Valore 2 SWAP: ";
    cin >> n2;
    swap(head, search_elem(head, n1), search_elem(head, n2));*/

    cout << endl;
    list head_2 = copy(head);
    cout << endl;
    print_list(head_2);
    delete_list(head);
    delete_list(head_2);
}