#include <iostream>

using namespace std;

int quadrato(int n)
{
    return n*n;
}

int cubo(int n)
{
    return n*n*n;
}

int somma(int n, int (*pf)(int)) {      //andrebbe anche bene int (pf)(int) oppure int pf(int)
	int sum = 0;
	for (int i = 1; i <= n; i++) {
		sum += pf(i);
	}
	return sum;
}


int main()
{
	cout << "Somma dei primi 4 quadrati: " << somma(4, quadrato) << endl;
	cout << "Somma dei primi 2 cubi: " << somma(2, cubo) << endl;

    int (*f[2])(int) = {quadrato, cubo};

    int d;
    cin >> d;
    cout << f[d](4) << endl; 
}