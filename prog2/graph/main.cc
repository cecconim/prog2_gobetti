#include <iostream>
#include <fstream>

using namespace std;

struct elemp
{
    int v;
    float w;
    elemp* pun;
};

typedef elemp* codap;


struct node
{
    int value;
    float weight;
    node* next;
};

//lista delle adiacenze
typedef node* adj_list;

//struttura del grafo
typedef struct{
    adj_list* nodes;    //array dinamico di liste di adiacenze
    int dim;            //dimensione dell'array dinamico
} graph;

//elemento lista
struct elemBFS
{
    int val;
    elemBFS* pun;
};

//struttura lista con testa e coda
typedef struct{elemBFS* head; elemBFS* tail;} list;

int min_queue(codap c){return c->v;}
elemp* tail_codap(codap c){return c->pun;}

//crea nuovo elemento della codap
elemp* new_elemp(int v, float w) {
	elemp *p = new elemp;
	p->v = v;
	p->w = w;
	p->pun = NULL;
	return p;
}

codap enqueue_codap(codap c, int v, float w)
{
    elemp* e = new_elemp(v, w);
    if(c==NULL || e->w < c->w)      //se la coda è vuota oppure la testa è più piccola del nuovo
    {
        e->pun = c;
        return e;
    }
    else
    {
        codap codap_copy = c;
        if(tail_codap(codap_copy) == NULL)
        {
            //significa che sono il secondo elemento
            c->pun = e;
            return c;
        }
        while(tail_codap(codap_copy)->pun != NULL && tail_codap(codap_copy)->w < e->w)
        {
            codap_copy = tail_codap(codap_copy);
        }
        e->pun = codap_copy->pun;
        codap_copy->pun = e;
        return c;
    }
}

int dequeue_codap(codap& c)
{
    int v = c->v;
    elemp* p = c;       //p punta alla testa
    c = c->pun;         //adesso la coda punta all'elemento dopo la testa(possibile perché passato con reference)
    
    delete p;           //cancello la vecchia testa
    return v;
}

bool is_empty_codap(codap c)
{
    if(c == NULL)
        return true;
    return false;
}

//POSSO SOLO ABBASSARE IL VALORE, PER QUESTO CI SONO MENO CONTROLLI!
codap decrease_priority(codap c, int i, float w)
{
    codap capp = c;
    if(c==NULL)
        return c;
    if(c->v == i)   //se il valore da cambiare è proprio la testa (NON C'è MODIFICA DI ORDINE!)
    {
        c->w = w;
        return c;
    }

    while(tail_codap(capp) != NULL)     //scorro la lista
    {
        if(tail_codap(capp)->v == i)    //se il successivo di capp è quello che voglio modificare
        {
            tail_codap(capp)->w = w;    //modifico il peso del successivo
            if(capp->w > tail_codap(capp)->w)  //se l'attuale dienta maggiore del successivo modificato
            {
                elemp* e = tail_codap(capp);
                capp->pun = e->pun;             //il precedente del modificato punta a quello dopo del modificato
                c = enqueue_codap(c, e->v, e->w);   //rimetto in coda il modificato
                delete e;                           //cancello il VECCHIO modificato
                return c;
            }
        }
        capp = tail_codap(capp);    
    }


    return c; //perché potrebbe cambiare il primo valore
}

int get_value(node* n){return n->value+1;}                      //+1 perché n funziona da pos, quindi il nodo in pos 4 è il 5
float get_weight(node* n){return n->weight;}
node* get_next(node* n){return n->next;}

int get_g_dim(graph g){return g.dim;}
adj_list get_g_adjlist(graph g, int n){return g.nodes[n-1];}    //-1 perché esempio: il nodo 5 è in posizione 4 

//arc  = con direzione
//edge = senza direzione
void add_arc(graph& g, int from, int n, float w)
{
    node* t = new node;
    t->value = n-1;
    t->weight = w;
    t->next = get_g_adjlist(g, from);
    g.nodes[from-1] = t;
}

void add_edge(graph& g, int from, int n, float w)
{
    add_arc(g, from, n, w);
    add_arc(g, n, from, w);
}

//crea un grafo con n nodi -> senza archi
graph new_graph(int n)
{
    graph g;
    g.dim = n;
    g.nodes = new adj_list[n];
    for(int i=0; i<n; i++)
        g.nodes[i] = NULL;
    return g;
}

elemBFS* new_elem(int n)
{
    elemBFS* elem = new elemBFS;
    elem->val = n;
    elem->pun = NULL;
    return elem;
}

list enqueue(list queue, int n)
{
    elemBFS* elem = new_elem(n);
    if(queue.tail != NULL)
        queue.tail->pun = elem;
    queue.tail = elem;
    if(queue.head == NULL)
        queue.head = queue.tail;
    return queue;
}

int dequeue(list &queue)
{
    int answ = (queue.head)->val;
    queue.head = (queue.head)->pun;
    return answ;
}

bool isEmpty(list queue)
{
    if(queue.head == NULL)
        return true;
    return false;
}

elemBFS* delete_elem(elemBFS* head, elemBFS* p)
{
    if(p == NULL)
        return head;
    if(head == p)
        head = head->pun;
    delete p;
    return head;
}

void delete_list(elemBFS* &head)
{
    while(head != NULL)
        head = delete_elem(head, head);
}

void print_BFS(graph g, int n)          //n = vortice da cui partire
{
    list queue = {NULL, NULL};
    bool visited[g.dim];
    for(int i=0; i<g.dim; i++)
        visited[i] = false;
    
    queue = enqueue(queue, n-1);
    visited[n-1] = true;

    int temp_q;
    node* temp_n;
    while(!isEmpty(queue))
    {
        temp_q = dequeue(queue);
        cout << temp_q+1 << ", " << endl;
        temp_n = g.nodes[temp_q];
        while(temp_n != NULL)
        {
            if(visited[temp_n->value] != true) //se non l'ho ancora visitato
            {
                visited[temp_n->value] = true;
                queue = enqueue(queue, temp_n->value);
            }
            temp_n = temp_n->next;
        }
    }
    delete_list(queue.head);
}

void inizialize(graph g, int from, float* dest, int* parent)
{
    for(int i=0; i<g.dim; i++)
    {
        dest[i] = 99;
        parent[i] = -1;
    }
    dest[from-1] = 0;
}

void relax(int u, int v, float w, float* dest, int* parent)
{
    /*
    u -> nodo di partenza   (POS)
    v -> valore vicino che punto
    w -> peso del valore vicino che punto
    dest -> lista dei pesi
    parent -> lista dei from
    */

    if(dest[v] > dest[u] + w)       //se il peso che attualmente che ho per arrivare a v è maggiore di quello
    {                               //per arrivare a u e dopo andare a v, allora il secondo è più corto
        dest[v] = dest[u] + w;
        parent[v] = u+1;            //+1 perché deve contenere il valore visivo
    }                              


	if (dest[v] > dest[u] + w) {
		dest[v] = dest[u] + w;
		parent[v] = u + 1;
	}
}

void dijkstra(graph g, int from)
{
    //trovo i cammini per tutti i nodi partendo da from

    float* dest = new float[g.dim];
    int* parent = new int[g.dim];
    
    inizialize(g, from, dest, parent);
    codap queue = NULL;
    for(int i=0; i<g.dim; i++)
        queue = enqueue_codap(queue, i, dest[i]);

    //metto nella coda tutti i nodi con il peso (utilizzato per prendere il più piccolo)

    while(!(is_empty_codap(queue)))
    {
        int u = dequeue_codap(queue);     //sicuramente il primo giro si prende la partenza
        //dovrò prendermi la lista delle agiacenze e capire se modificare
        adj_list neigh = get_g_adjlist(g, u+1);  //dentro la funzione c'è -1 quindi devo fare +1 qui (rischio seg def con -1)
        while(neigh != NULL)        //neigh è la lista delle agiacenze, va letta tutta
        {
            int v = get_value(neigh);           //valore puntato 
            float w_u_v = get_weight(neigh);    //peso valore puntato
            relax(u, v-1, w_u_v, dest, parent); //v-1 perché V contiene il valore visivo
            queue = decrease_priority(queue, v-1, dest[v-1]);   //aggiorno il valore nella coda
            neigh = get_next(neigh);
        }
    }

    cout << "Nodo\t Peso \t Padre" << endl;
    for (int i = 0; i < g.dim; i++) {
        cout << i + 1 << " \t " << dest[i] << " \t " << parent[i] << endl;
    }
    delete[] dest;
    delete[] parent;
    return;
}



int main(int argc, char *argv[])
{
    ifstream s;
    s.open(argv[1]);
    int size;
    int v1, v2;
    float w1;
    s >> size;
    graph g = new_graph(size);
    while(s >> v1 >> v2 >> w1)
    {
        add_edge(g, v1, v2, w1);
    }
    s.close();
    print_BFS(g, 1);
    //dijkstra(g, 1);
}


/**
 * 1 4 5 1
 * 4 3 1 0
 * 3 1 2 1
 */