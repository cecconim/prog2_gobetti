#include <iostream>

using namespace std;


struct node
{
    string name;
    node* parent;
    node* firstChild;
    node* nextSibling;
};

struct elemBFS
{
    node* val;
    elemBFS* pun;
};

typedef struct{elemBFS* head; elemBFS* tail;} list;

typedef node* tree;

typedef int valore;

string get_name(node* n){return n->name;}
node* get_parent(node* n){return n->parent;}
node* get_child(node* n){return n->firstChild;}
node* get_sibling(node* n){return n->nextSibling;}

node* create_node()
{
    node* n = new node;
    cout << "Inserire valore: ";
    cin >> n->name;
    n->firstChild = NULL;
    n->nextSibling = NULL;
    n->parent = NULL;
    return n;
}

node* get_last_sibling(node* n)   //prendo l'ultimo fratello del nodo passato
{
    if(get_sibling(n) == NULL)
        return n;
    return get_last_sibling(get_sibling(n));
}

//creo un fratello del nodo passato
void create_sibling(node* n)
{
    node* p = create_node();
    p->parent = n->parent;                      //hanno lo stesso padre
    (get_last_sibling(n))->nextSibling = p;     //diventa fratello dell'ultimo fratello del nodo passato
    //n->nextSibling = p;                       //diventa fratello del nodo passato (potrebbe dare errori!)
    return;
}

//creo un figlio del nodo passato
void create_child(node* n)
{
    node* p = create_node();
    p->parent = n;                                          //il padre del nuovo nodo è il nodo passato
    if(get_child(n) == NULL)                                //SE NON HA FIGLI, DIVENTA IL PRIMO FIGLIO
        n->firstChild = p;
    else
        (get_last_sibling(n->firstChild))->nextSibling = p; //l'ultimo fratello del primo figlio (quindi l'ultimo figlio)
    return;                                                 //del nodo passato punterà al nuovo puntatore
}

tree create_tree()
{
    return create_node();   
}

int menu()
{
    int n;
    do
    {
        cout << endl;
        cout << "1: Crea albero" << endl;
        cout << "2: Fratello" << endl;
        cout << "3: Figlio" << endl;
        cout << "4: Stampa DFS(depth-first search)" << endl;
        cout << "5: Stampa BFS(breath-first search)" << endl;
        cout << "COMANDO: ";
        cin >> n;
    } while (n<1 || n>5);
    return n;
}

void print_DFS(tree root)
{
    cout << get_name(root) << "(";
    if(get_child(root) != NULL)
        print_DFS(get_child(root));
    cout << ") ";
    if(get_sibling(root) != NULL)
        print_DFS(get_sibling(root));
    return;
}

//CERCO CON UNA DFS
node* search(tree root, string name)
{
    node* f = NULL;
    if(get_name(root)==name)
        return root;
    if(get_child(root) != NULL)
        f = search(get_child(root), name);
    if(get_sibling(root) != NULL)
        f = search(get_sibling(root), name);
    return f;
}

elemBFS* new_elem(node* n)
{
    elemBFS* elem = new elemBFS;
    elem->val = n;
    elem->pun = NULL;
    return elem;
}

list enqueue(list queue, node* node)
{
    elemBFS* elem = new_elem(node);
    //cout << "INCODO: " << elem->val->name << endl;
    if(queue.tail != NULL)          //se la coda della lista non è null, allora l'elemento in coda punta al nuovo elem
        queue.tail->pun = elem;
    queue.tail = elem;              //la nuovo elem diventa la coda
    //cout << "NUOVA CODA: " << queue.tail->val->name << endl;
    if(queue.head == NULL)          //se la testa è vuota, allora la testa è anche la coda
        queue.head = queue.tail;
    //cout << "TESTA: " << queue.head->val->name << endl;
    return queue;
}

//restituisco il nodo che tolgo dalla lista
node* dequeue(list &queue)
{
    node* answ=(queue.head)->val;    //infatti restituisco il valore che avevo messo in testa
    queue.head = (queue.head)->pun; //e il nuovo valore in testa è quello che precedentenemente puntava la testa
    return answ;
}

bool isEmpty(list queue)
{
    if(queue.head == NULL)
        return true;
    return false;
}

elemBFS* delete_elem(elemBFS* head, elemBFS* p)
{
    if(p == NULL)
        return head;
    if(head == p)
    {
        head = head->pun;
    }
    delete p;
    return head;
}

void delete_list(elemBFS* &head)
{
    while(head != NULL)
    {
        head = delete_elem(head, head);
    }
}

//elemBFS, list
void print_BFS(tree root)
{
    list queue = {NULL, NULL};
    queue = enqueue(queue, root);           //metto in coda primo valore!
    //cout << "Queue HEAD: " << queue.head << endl << "Queue TAIL: " << queue.tail << endl << endl;
    while(!isEmpty(queue))                  //se la coda non è vuota allora continuo
    {
        root = dequeue(queue);
        cout << get_name(root) << ", ";
        if(get_child(root) != NULL) //se ha figli allora continuo
        {
            root = get_child(root);     //root diventa il suo primo figlio
            while(root != NULL)
            {
                queue = enqueue(queue, root);   //metto in coda il figlio
                root = get_sibling(root);   //vado al prossimo fratello
            }
        }
    }
    delete_list(queue.head);
    //cout << "Queue HEAD: " << queue.head << endl << "Queue TAIL: " << queue.tail << endl << endl;
}


//IL MAIN NON TIENE CONTO SE ALBERO GIà CREATO O NO, FARE FUTURI CONTROLLI
//ES: NON PUOI INSERIRE FRATELLI O SORELLE SE L'ALBERO NON ESISTE
//ES: NON PUOI CREARE DI NUOVO L'ALBERO SE NE ESISTE GIà UNO!
int main()
{
    int ans;
    tree root = NULL;
    node* n;
    string s;
    while(true)
    {
        ans = menu();
        switch(ans)
        {
            case 1:
                root = create_tree();
                break;
            case 2:
                cout << "Inserire nome da cercare: ";
                cin >> s;
                n = search(root, s);
                if(n == NULL)
                {
                    cout << "Elemento non trovato!";
                    break;
                }
                create_sibling(n);
                break;
            case 3:
                cout << "Inserire nome da cercare: ";
                cin >> s;
                n = search(root, s);
                if(n == NULL)
                {
                    cout << "Elemento non trovato!";
                    break;
                }
                create_child(n);
                break;
            case 4:
                print_DFS(root);
                break;
            case 5:
                print_BFS(root);
                break;
        }
    }
}